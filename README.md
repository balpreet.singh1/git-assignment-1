# Git Assignment - 1


# Create a folder ninja at the root level of your cloned code

![](Screenshots/Screenshot1.png)

# Add a file README.md with content "Trying fast forward merge"

![](Screenshots/Screenshot2.png)

# Create a branch ninja and move to it, Run git status command, Commit your changes to ninja branch

![](Screenshots/Screenshot3.png)

# Merge ninja branch to master branch make sure that a new commit get's created

![](Screenshots/Screenshot4.png)

# Assuming you are in the master branch, modify README.md with content Changes in master branch, commit the changes in master branch.

![](Screenshots/Screenshot5.png)

# Switch to ninja branch, modify README.md with content Changes in ninja branch, commit the changes in ninja branch.

![](Screenshots/Screenshot6.png)
![](Screenshots/Screenshot7.png)
![](Screenshots/Screenshot8.png)

# Merge the ninja branch to the branch in such a fashion that a merge conflict is generated and resolve it using the ours and theirs concept so that the changes of the ninja branch overrides changes in master branch

![](Screenshots/Screenshot9.png)
![](Screenshots/Screenshot10.png)
![](Screenshots/Screenshot11.png)
![](Screenshots/Screenshot12.png)
![](Screenshots/Screenshot13.png)

# Revert the above merge commit

![](Screenshots/Screenshot16.png)
![](Screenshots/Screenshot17.png)

# Merge master branch to ninja branch in such a fashion that changes of both branches gets accumulated.
![](Screenshots/Screenshot18.png)
![](Screenshots/Screenshot15.png)
![](Screenshots/Screenshot14.png)




